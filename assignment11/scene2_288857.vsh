#version 150

in vec4 aPosition;
in vec3 aColor;
in float aSplatSize;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

uniform float uScaleToScreen;

out vec3 vColor;


void main() {

    // Pass color to fragment shader
    // Note: No bi-linear interpolation will be applied
    // since we're rendering points instead of triangles
    vColor = aColor;
    
    // =======================================================================
    // =======================================================================
    // Assignment code:
    // Part B:
    // =======================================================================
    // =======================================================================

	vec4 eyePosition = uModelViewMatrix * aPosition;
	gl_PointSize = aSplatSize * uScaleToScreen/eyePosition.z;
    gl_Position = uProjectionMatrix * eyePosition;
	
    
    // =======================================================================
    // End assignment code
    // =======================================================================
}
