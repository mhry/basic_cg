#version 150

in vec4 vPosition;
in vec3 vNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

uniform vec3 uLightPosition;
uniform vec3 uLightColor;
uniform float uLightSpotLightFactor;

uniform mat3 uAmbientMaterial;
uniform mat3 uDiffuseMaterial;
uniform mat3 uSpecularMaterial;
uniform float uSpecularityExponent;

out vec4 oFragColor;

void main() {
    vec3 normal = normalize(vNormal);
    vec3 posToLight = uLightPosition - vec3(vPosition);
    vec3 posToLightDir = normalize(posToLight);
    vec3 posToEye = -vPosition.xyz;
    vec3 posToEyeDir = normalize(posToEye);
    vec3 reflectDir = normalize(2 * outerProduct(normal, normal) * posToLight - posToLight);
    vec3 spotLightDir = normalize((uModelViewMatrix * vec4(0, 0, 0, 1)).xyz - uLightPosition);
    
    float spotLightFactor = pow(max(0, dot(spotLightDir, -posToLightDir)), uLightSpotLightFactor);
    float diffuse = max(0, dot(posToLightDir, normal));
    float specular = pow(max(0, dot(reflectDir, posToEyeDir)), uSpecularityExponent);
    
    vec3 C_a = uLightColor * uAmbientMaterial;
    vec3 C_d = uLightColor * uDiffuseMaterial * diffuse * spotLightFactor;
    vec3 C_s = uLightColor * uSpecularMaterial * specular * spotLightFactor;
    
    oFragColor = vec4(C_a + C_d + C_s, 1);
}
