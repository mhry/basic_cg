#version 150

in vec4 aPosition;
in vec3 aNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

out vec4 vPosition;
out vec3 vNormal;

void main() {
    mat4 normalMatrix = inverse(transpose(uModelViewMatrix));
    vPosition = uModelViewMatrix * aPosition;
    vNormal = vec3(normalMatrix * vec4(aNormal, 0));
    gl_Position = uProjectionMatrix * vPosition;
}
