#version 150

in vec4 aPosition;
in vec3 aNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

uniform vec3 uLightPosition;
uniform vec3 uLightColor;

uniform mat3 uAmbientMaterial;
uniform mat3 uDiffuseMaterial;
uniform mat3 uSpecularMaterial;
uniform float uSpecularityExponent;

out vec3 vColor;

void main() {
    mat4 normalMatrix = inverse(transpose(uModelViewMatrix));

    vec4 esPosition = uModelViewMatrix * aPosition;
    vec3 esNormal = normalize(vec3(normalMatrix * vec4(aNormal, 0)));
    
    vec3 posToLight = uLightPosition - vec3(esPosition);
    vec3 posToLightDir = normalize(posToLight);
    vec3 posToEye = -esPosition.xyz;
    vec3 posToEyeDir = normalize(posToEye);
    vec3 reflectDir = normalize(2 * outerProduct(esNormal, esNormal) * posToLight - posToLight);
    
    float diffuse = max(0, dot(posToLightDir, esNormal));
    float specular = pow(max(0, dot(reflectDir, posToEyeDir)), uSpecularityExponent);
    
    vec3 C_a = uLightColor * uAmbientMaterial;
    vec3 C_d = uLightColor * uDiffuseMaterial * diffuse;
    vec3 C_s = uLightColor * uSpecularMaterial * specular;
    
    vColor = C_a + C_d + C_s;
    gl_Position = uProjectionMatrix * esPosition;
}
