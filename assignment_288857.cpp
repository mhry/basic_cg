/*
 * Basics of Computer Graphics Exercise
 */
 
#include "assignment.h"
#include <glm/ext.hpp>
using namespace std;
using namespace glm;


// lifted from main.cpp
#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

mat4 ellipsisMatrix(vec2 pos, vec2 scale, float angle);

/* Racecar that only moves on a circle */
class Racecar {
public:	
	vec3 color;
	float pos_rad;
	float radius, omega; // radius and radial speed (full circles/sec)	
	const static vec2 size;
	void drawAndMove(float dt) {		
		vec2 pos(radius * cos(pos_rad), radius * sin(pos_rad));
		drawCircle(color, ellipsisMatrix(pos, size, M_PI/2 - pos_rad));
		pos_rad -= fmod((omega * dt), 2*M_PI); // prevent hypothetical overflow
	}
};

const vec2 Racecar::size = vec2(0.08, 0.02);


// add your team members names and matrikel numbers here:
void printStudents()
{
	cout << "Markus Hrywniak, 288857" << endl;
	cout << "Kaspar Scharf, 321503" << endl;
	cout << "Tobias Gerarts, 301447" << endl;
	cout << "Simon Oehrl, 310609" << endl;
}

/* Create a scaling matrix */
mat4 scaleMatrix(vec2 scale) {
	mat4 m(1.f);
	m[0][0] = scale.x;
	m[1][1] = scale.y;
	return m;
}

/* Create a translation matrix that adds "pos" to all points */
mat4 transMatrix(vec2 pos) {
	mat4 m(1.f);
	m[3] = vec4(pos.x, pos.y, 0, 1.);
	return m;
}

/* Rotate in the 2D xy plane by phi radians */
mat4 rotateMatrix2D(float phi) {
	mat4 rotMat(1.);
	rotMat[0][0] = glm::cos(phi);
	rotMat[0][1] = -glm::sin(phi);
	rotMat[1][0] = glm::sin(phi);
	rotMat[1][1] = glm::cos(phi);
	return rotMat;
}

/* Draw an ellipsis (scaled circle), rotate it and displace it (in that order) */
mat4 ellipsisMatrix(vec2 pos, vec2 scale, float angle) {
	mat4 mat_scale = scaleMatrix(scale),
		 mat_move = transMatrix(pos);	
	return mat_move * rotateMatrix2D(angle) * mat_scale;
}

/* Draw the separation between both lanes as a series of `num_bars` ellipses
 * on the circle with radius `r`.
 * Algorithm: 
 * 1) Determine offset between bars in radians, set length of bars.
 * 2) Draw Ellipsis in the right form pointing along y-axis at the center of the viewport.
 * 3) Determine its position on the circle (polar coords)
 * 4) Rotate it to be a tangent to the circle at that position and move it over there.
 */
void drawLaneSeparator(float r, int num_bars) {	
	float d_bar = 2*M_PI/num_bars;
	float bar_len = .023;
	vec2 bar_size(.009, bar_len);
	for (int i = 0; i < num_bars; ++i) {	
		float phi = M_PI/30 + i * d_bar;
		vec2 pos(r * cos(phi), r * sin(phi));
		// tangent: rotation angle is negative of polar angle
		drawCircle(vec3(1, 1, 1), ellipsisMatrix(pos, bar_size, -phi));
	}
}

/* Draw a circle around the origin with radius r */
void drawCircleRadius(vec3 color, float r) {
	drawCircle( color, scaleMatrix(vec2(r)));
}

void drawScene(int scene, float runTime) {
	static Racecar car1 = { vec3(1, 1, 0), 0, 0, M_PI/4 }, 
		car2 = { vec3(0, 1, 0), 0, 0, 2 * car1.omega };
	static float lastTime = runTime;
	vec3 color_gray_track(.3f, 0.3f, 0.3f);
	float outer_radius = .8, inner_radius = .6, border_thickness = .01;
	// outer blue circle		
	drawCircleRadius( glm::vec3(0.0f, 0.0f, 1.0f), outer_radius);
	// gray track	
	drawCircleRadius( color_gray_track,  outer_radius - border_thickness);
	// inner blue circle
	drawCircleRadius( glm::vec3(0.0f, 0.0f, 1.0f), inner_radius);
	// black middle	
	drawCircleRadius( glm::vec3(0.0f, 0.0f, 0.0f), inner_radius - border_thickness);

	// b) mighty ellipsis of spectatorism
	drawCircle( color_gray_track + .1f, ellipsisMatrix(vec2(-.9, 0), vec2(.05, .6), 0));
	// c) finish line
	int num_stripes = 9;
	float spacing = (outer_radius - inner_radius) / num_stripes;
	// extra spacing because center of ellipsis should lie on race track
	for (int i = 0; i < num_stripes; ++i)
		drawCircle( vec3(1, 1, 1), 
			ellipsisMatrix(vec2(-outer_radius + 2*border_thickness + spacing*i, 0), 
			vec2(.008, .04), 0));
	// d) lane separator
	float r_middle = inner_radius + ((outer_radius - border_thickness) - inner_radius)/2, 
		  circ_middle = 2*M_PI*r_middle;
	drawLaneSeparator(r_middle, 30);
	// e) race cars	
	float dt = runTime - lastTime;
	lastTime = runTime;
	car1.radius = (r_middle + inner_radius) / 2;
	car2.radius = (r_middle + (outer_radius - border_thickness)) / 2;
	car1.drawAndMove(dt); // inner (yellow) car
	car2.drawAndMove(dt); // outer (green) car

	// extra: game mode
	if (scene == 3) {
		vec3 col(1, 0, 0);
		if (arrowKeyUpPressed)
			drawCircle(col, ellipsisMatrix(vec2(0, .1), vec2(.4), 0));
		else if (arrowKeyDownPressed)
			drawCircle(col, ellipsisMatrix(vec2(0, -.1), vec2(.4), 0));
		else
			drawCircleRadius(vec3(1, 0, 0), .4);
	}
}

void initCustomResources()
{
}

void deleteCustomResources()
{
}

