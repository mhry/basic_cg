/*
 * Basics of Computer Graphics Exercise
 */
 
#include "assignment.h"
using namespace std;

// add your team members names and matrikel numbers here:
void printStudents()
{
	cout << "Markus Hrywniak, 288857" << endl;
	cout << "Kaspar Scharf, 321503" << endl;
	cout << "Tobias Gerarts, 301447" << endl;
	cout << "Simon Oehrl, 310609" << endl;
}

void drawScene( int scene, float runTime )
{
    
}

void initCustomResources()
{
	//
    // Print some general information about the supported OpenGL, the GPU vendor and sometimes the driver version:
    //
    cout << "OpenGL context version: " << glGetString(GL_VERSION)  << endl;
    cout << "OpenGL vendor:          " << glGetString(GL_VENDOR)   << endl;
    cout << "OpenGL renderer:        " << glGetString(GL_RENDERER) << endl;
    cout << "GLSL version:           " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}

void deleteCustomResources()
{
}

