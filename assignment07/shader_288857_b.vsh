#version 150

uniform float time;

in vec4 position;
in vec3 color;

out vec3 vColor;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

void main() {
    vec4 pos = position;

    // Add your code here
	// sin(x)^2 is in [0, 1]
	pos.xyz *= .33 + .67*pow(sin(time),2);
	gl_Position = projectionMatrix * modelViewMatrix * pos;
    // End of your code here

    vColor  = color;
}
