/*
 * Basics of Computer Graphics Exercise
 */

#include "assignment.h"
using namespace std;

extern GLuint teapot_vbo;
extern GLuint teapot_ea;

GLuint vs_a, fs_a, prog_a;
GLuint vs_b, fs_b, prog_b;
GLuint vs_c, fs_c, prog_c;

// Precompute inverse transposed model-view matrix
glm::mat4 invTranspModelView;

extern glm::mat4 g_ModelViewMatrix;
extern glm::mat4 g_ProjectionMatrix;

// vertex data as defined in vertexData.cpp:
// number of triangles:
int trianglesInVBOs = 1984;

//// per vertex: x,y,z
extern float spherePosition[];

//// per vertex: nx,ny,nz, cr,cg,cb
extern float sphereNormalColor[];

//// per vertex: x,y,z, nx,ny,nz, cr,cg,cb
extern float konus[];


GLuint vboSpherePos;
GLuint vboSphereNormalColor;
GLuint vboKonus;

GLuint vaoA, vaoB, vaoC;



// add your team members names and matrikel numbers here:
void printStudents()
{
	cout << "Markus Hrywniak, 288857" << endl;
	cout << "Kaspar Scharf, 321503" << endl;
	cout << "Tobias Gerarts, 301447" << endl;
	cout << "Simon Oehrl, 310609" << endl;
}

void drawScene(int scene, float runTime) {

    // Create modelview matrix
    // First: Create transformation into eye-space
    // glm::lookAt needs the eye position, the center to look at and the up vector
    g_ModelViewMatrix = glm::lookAt(eyePos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    
    // One round should take 10 seconds
    float angle = runTime * 360.0f / 10.0f;

    glm::mat4 modelMatrix = glm::rotate(angle, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix *= glm::scale(1.0f, 1.3f, 1.0f);

    g_ModelViewMatrix = g_ModelViewMatrix * modelMatrix;

    if(scene == 1) {

      // ====================================================================
      // Add your code here:
      // Scene A
      // ====================================================================
      
      glUseProgram(prog_a);
      GLint projectionMatrixLocation = glGetUniformLocation( prog_a, "projectionMatrix" );
      GLint modelViewMatrixLocation  = glGetUniformLocation( prog_a, "modelViewMatrix" );
	  glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));
	  glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ModelViewMatrix));
	  glBindVertexArray(vaoA);
	  glDrawArrays(GL_TRIANGLES, 0, 3*trianglesInVBOs);	 



      // ====================================================================
      // End Exercise code
      // ====================================================================


    } else if(scene == 2) {
      
      // ====================================================================
      // Add your code here:
      // Scene B
      // ====================================================================

      glUseProgram(prog_b);
      GLint projectionMatrixLocation = glGetUniformLocation( prog_b, "projectionMatrix" );
      GLint modelViewMatrixLocation  = glGetUniformLocation( prog_b, "modelViewMatrix" );
      GLint timeLocation             = glGetUniformLocation( prog_b, "time" );
	  glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));
	  glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ModelViewMatrix));
	  glUniform1f(timeLocation, runTime);
	  glBindVertexArray(vaoB);
	  glDrawArrays(GL_TRIANGLES, 0, 3*trianglesInVBOs);	 


      // ====================================================================
      // End Exercise code
      // ====================================================================

      
    } else if(scene == 3) {

      // ====================================================================
      // Add your code here:
      // Scene C
      // ====================================================================

      glUseProgram(prog_c);
      GLint projectionMatrixLocation = glGetUniformLocation( prog_c, "projectionMatrix" );
      GLint modelViewMatrixLocation  = glGetUniformLocation( prog_c, "modelViewMatrix" );
      GLint timeLocation             = glGetUniformLocation( prog_c, "time" );
	  glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));
	  glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ModelViewMatrix));
	  glUniform1f(timeLocation, runTime);
	  glBindVertexArray(vaoC);
	  glDrawArrays(GL_TRIANGLES, 0, 3*trianglesInVBOs);	 

      // ====================================================================
      // End Exercise code
      // ====================================================================

      
    } else if(scene == 4) {

      // ====================================================================
      // Add your code here:
      // Scene D
      // ====================================================================
	  // custom modelViewMatrix: view from positive z-direction to the origin
	  glm::vec3 myEye(0.f, -3.0f, 0.0f);
	  glm::mat4 modelMatD = glm::lookAt(myEye, glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));
	  // offsets of the two objects
	  float offset_sphere = 1.5f;
	  float offset_cylinder = -offset_sphere;

	  // draw the sphere on the right
	  {
	  glUseProgram(prog_c);	 
	  GLint projectionMatrixLocation = glGetUniformLocation( prog_c, "projectionMatrix" );
      GLint modelViewMatrixLocation  = glGetUniformLocation( prog_c, "modelViewMatrix" );
      GLint timeLocation             = glGetUniformLocation( prog_c, "time" );	  
	  glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));  
	  glm::mat4 modelMatSphere = glm::translate(glm::vec3(offset_sphere, 0, 0)) * modelMatD;
	  glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatSphere));
	  glUniform1f(timeLocation, runTime);	   
	  glBindVertexArray(vaoC);
	  glDrawArrays(GL_TRIANGLES, 0, 3*trianglesInVBOs);	 
	  }
	  // draw the cylinder on the left
	  {
	  glUseProgram(prog_b);
      GLint projectionMatrixLocation = glGetUniformLocation( prog_b, "projectionMatrix" );
      GLint modelViewMatrixLocation  = glGetUniformLocation( prog_b, "modelViewMatrix" );
      GLint timeLocation             = glGetUniformLocation( prog_b, "time" );
	  glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));  
	  glm::mat4 modelMatCylinder = glm::translate(glm::vec3(offset_cylinder, 0, 0)) * modelMatD;
	  glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatCylinder));
	  glUniform1f(timeLocation, runTime);
	  glBindVertexArray(vaoB);
	  glDrawArrays(GL_TRIANGLES, 0, 3*trianglesInVBOs);	
	  }



      // ====================================================================
      // End Exercise code
      // ====================================================================

      
    } 
        
}

void initCustomResources() {

    // ====================================================================
    // create your ressources here, e.g. shaders, buffers,...
    // ====================================================================


    // create the shaders:
    createShaderProgram(vs_a, fs_a, prog_a, "shader_288857_a.vsh", "shader_288857_a.fsh");
    createShaderProgram(vs_b, fs_b, prog_b, "shader_288857_b.vsh", "shader_288857_b.fsh");
    createShaderProgram(vs_c, fs_c, prog_c, "shader_288857_c.vsh", "shader_288857_c.fsh");

    
    // ====================================================================
    // VBOs:
    // ====================================================================
    glGenBuffers( 1, &vboSpherePos );
    glBindBuffer( GL_ARRAY_BUFFER, vboSpherePos );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float)*trianglesInVBOs*3*3, // 3 vertices per triangle, 3 floats per vertex
                  spherePosition, GL_STATIC_DRAW );

    glGenBuffers( 1, &vboSphereNormalColor);
    glBindBuffer( GL_ARRAY_BUFFER, vboSphereNormalColor );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float)*trianglesInVBOs*3*6, // 3 vertices per triangle, 6 floats per vertex
                 sphereNormalColor, GL_STATIC_DRAW );

    glGenBuffers( 1, &vboKonus );
    glBindBuffer( GL_ARRAY_BUFFER, vboKonus );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float)*trianglesInVBOs*3*9, // 3 vertices per triangle, 9 floats per vertex
                 konus, GL_STATIC_DRAW );


    
    // ====================================================================
    // Add your code here:
    // Generate VertexArrayObjects for the scenes
    // Scene A
    // ====================================================================
	{
	GLint pos_location = -1;
	glGenVertexArrays(1, &vaoA);
	glBindVertexArray(vaoA);		
	
	// sphere position
	glBindBuffer( GL_ARRAY_BUFFER, vboSpherePos );
	pos_location = glGetAttribLocation(prog_a, "position");
	// 3 floats: X, Y, Z, tightly packed -> stride 0
	glVertexAttribPointer(pos_location, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(pos_location);	
	}

    // ====================================================================
    // Add your code here:
    // Generate VertexArrayObjects for the scenes
    // Scene B
    // ====================================================================
	{
	GLint color_location = -1, pos_location = -1;	
	glGenVertexArrays(1, &vaoB);
	glBindVertexArray(vaoB);
	pos_location   = glGetAttribLocation(prog_b, "position");
	color_location = glGetAttribLocation(prog_b, "color");
	// sphere position: as before
	glBindBuffer( GL_ARRAY_BUFFER, vboSpherePos );	
	glVertexAttribPointer(pos_location, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(pos_location);	

	// sphere color: from other buffer
	glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormalColor);
	// color vbo: 3 floats normals, 3 floats colors -> ignore the normals, hence stride 6 and offset 3
	glVertexAttribPointer(color_location, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), reinterpret_cast<void*>(3*sizeof(float)));
	glEnableVertexAttribArray(color_location);
	}


    // ====================================================================
    // Add your code here:
    // Generate VertexArrayObjects for the scenes
    // Scene C
    // ====================================================================
	{
	GLint colorS_loc, colorK_loc, posS_loc, posK_loc;
	glGenVertexArrays(1, &vaoC);
	glBindVertexArray(vaoC);
	colorS_loc = glGetAttribLocation(prog_c, "colorS");
	colorK_loc = glGetAttribLocation(prog_c, "colorK");
	posS_loc = glGetAttribLocation(prog_c, "positionS");
	posK_loc = glGetAttribLocation(prog_c, "positionK");

	// sphere position: as before
	glBindBuffer( GL_ARRAY_BUFFER, vboSpherePos );	
	glVertexAttribPointer(posS_loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(posS_loc);	
	// colors for sphere: as in part b)
	glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormalColor);
	glVertexAttribPointer(colorS_loc, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), reinterpret_cast<void*>(3*sizeof(float)));
	glEnableVertexAttribArray(colorS_loc);

	// now: cylinder
	glBindBuffer(GL_ARRAY_BUFFER, vboKonus);
	// 9 floats per vertex: first 3 are position	
	glVertexAttribPointer(posK_loc, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float), 0);
	glEnableVertexAttribArray(posK_loc);
	// last 3 are colors
	glVertexAttribPointer(colorK_loc, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float), reinterpret_cast<void*>(6*sizeof(float)));
	glEnableVertexAttribArray(colorK_loc);
	}
	

		




    // ====================================================================
    // End Exercise code
    // ====================================================================


}

void deleteCustomResources() {
    // don't forget to delete your OpenGL ressources (shaders, buffers, etc.)!

    glDeleteShader(vs_a);
    glDeleteShader(vs_b);
    glDeleteShader(vs_c);
    glDeleteShader(fs_a);
    glDeleteShader(fs_b);
    glDeleteShader(fs_c);
    glDeleteProgram(prog_a);
    glDeleteProgram(prog_b);
    glDeleteProgram(prog_c);
	glDeleteVertexArrays(1, &vaoA);
	glDeleteVertexArrays(1, &vaoB);
	glDeleteVertexArrays(1, &vaoC);
}

