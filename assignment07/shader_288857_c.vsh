#version 150

in vec4 positionS;
in vec4 positionK;
in vec3 colorS;
in vec3 colorK;

out vec3 vColor;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform float time;

void main() {
	float pS = pow(sin(time), 2);
	float pK = 1. - pS;
	vec4 pos = pS*positionS + pK*positionK;
	vec3 col = pS*colorS + pK*colorK;
   	gl_Position = projectionMatrix* modelViewMatrix * pos;
	vColor = col;
}
