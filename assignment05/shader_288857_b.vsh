#version 150

in vec4 aPosition;
in vec3 aNormal;
out vec3 color;

uniform mat4 projMatrix;
uniform mat4 modViewMatrix;

void main() {
	gl_Position = projMatrix*modViewMatrix*aPosition;
	color = aNormal;
}