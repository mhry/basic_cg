#version 150

in vec4 aPosition;
in vec3 aColor;
out vec3 color;

uniform mat4 modViewMatrix;
uniform mat4 projMatrix;

void main() {
	gl_Position = projMatrix*modViewMatrix*aPosition;	
	color = aColor;
}