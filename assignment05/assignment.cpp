/*
 * Basics of Computer Graphics Exercise
 */
 
#include "assignment.h"
#include <fstream>
using namespace std;

extern int g_numberOfBunnyVertices;
extern int g_bunnyStrideSize;
extern float g_bunnyMesh[];
extern int g_bunnyColorStrideSize;
extern unsigned char g_bunnyColor[];

// add your team members names and matrikel numbers here:
void printStudents()
{
	cout << "Markus Hrywniak, 288857" << endl;
	cout << "Kaspar Scharf, 321503" << endl;
	cout << "Tobias Gerarts, 301447" << endl;
	cout << "Simon Oehrl, 310609" << endl;
}

glm::mat4 g_ModelViewMatrix;
glm::mat4 g_ProjectionMatrix;

glm::mat4 buildFrustum( float phiInDegree, float aspectRatio, float near, float far) {

    float phiHalfInRadians = 0.5f*phiInDegree * (M_PI/180.0f);
    float t = near * tan( phiHalfInRadians );
    float b = -t;
    float left = b*aspectRatio;
    float right = t*aspectRatio;

	return glm::frustum( left, right, b, t, near, far );
}

void resizeCallback( int newWidth, int newHeight )
{
	// define the part of the screen OpenGL should draw to (in pixels):
    glViewport( 0, 0, newWidth, newHeight );
    g_ProjectionMatrix = buildFrustum(90.0f, (newWidth/(float)newHeight), 0.1f, 100.0f);
}


// ==============================================
// Global varibles for binding the vbo
// ==============================================
GLuint bunny_vbo;
GLuint bunny_color_vbo;

// ==============================================
// Global varibles for the shaders.
// ==============================================
GLuint vs_a, fs_a, prog_a;
GLuint vs_b, fs_b, prog_b;
GLuint vs_c, fs_c, prog_c;

void drawScene(int scene, float runTime) {
	
	// glm::lookAt needs the eye position, the center to look at and the up vector, so it works a bit different 
	// than our lookAt from last week:
	glm::vec3 pos = glm::vec3(1.5f*cos(runTime), 0.0f, 1.5f*sin(runTime) );
	g_ModelViewMatrix = glm::lookAt( pos, glm::vec3(0,0,0), glm::vec3(0,1,0) );
	glm::mat4 modelMatrix = glm::translate( 0.0f, 0.0f, 0.35f );
	g_ModelViewMatrix = g_ModelViewMatrix * modelMatrix;
	
	if (scene == 1) {

        // ====================================================================
        // assignment part a
        // Add your code here:
        // ====================================================================
		glUseProgram(prog_a);		

        // ====================================================================
        // End Exercise code
        // ====================================================================

	} else if (scene == 2) {
		
        // ====================================================================
        // assignment part b
        // Add your code here:
        // ====================================================================
		glUseProgram(prog_b);
		// transfer matrices into shader
		GLint modViewLoc = glGetUniformLocation(prog_b, "modViewMatrix");
		GLint projMatLoc = glGetUniformLocation(prog_b, "projMatrix");
		glUniformMatrix4fv(modViewLoc, 1, GL_FALSE, glm::value_ptr(g_ModelViewMatrix));
		glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));
        // ====================================================================
        // End Exercise code
        // ====================================================================


	} else if (scene == 3) {
		
        // ====================================================================
        // assignment part c
        // Add your code here:
        // ====================================================================
		// only difference: use program c, which contains a different frag shader
		glUseProgram(prog_c);	
		GLint modViewLoc = glGetUniformLocation(prog_c, "modViewMatrix");
		GLint projMatLoc = glGetUniformLocation(prog_c, "projMatrix");
		glUniformMatrix4fv(modViewLoc, 1, GL_FALSE, glm::value_ptr(g_ModelViewMatrix));
		glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, glm::value_ptr(g_ProjectionMatrix));

        // ====================================================================
        // End Exercise code
        // ====================================================================
	}
	glDrawArrays(GL_TRIANGLES, 0, g_numberOfBunnyVertices);
}

void createShaderProgram( GLuint &vs, GLuint &fs, GLuint &prog, const std::string &vsFileName, const std::string &fsFileName ) {

    // =========================================================================
    // create your shaders here
    //
    // Input are the filenames of the vertex and the fragment shader.
    //
    // You have to return the handles of the vertex shader, fragment shader
    // and the final program.
    //
    // Code required for a,b,c
    //
    // Add your code here:
    // ====================================================================
    vs = glCreateShader(GL_VERTEX_SHADER);
    fs = glCreateShader(GL_FRAGMENT_SHADER);
    prog = glCreateProgram();
    // load shader source into array
	string vsrc = getFileContent(vsFileName);
	string fsrc = getFileContent(fsFileName);
	const char *vp = vsrc.c_str(), *fp = fsrc.c_str();
	glShaderSource(vs, 1, &vp, NULL);
    glShaderSource(fs, 1, &fp, NULL);

    // compile everything and attach the shaders
    glCompileShader(vs);
	printGLSLCompileLog( vs );
    glCompileShader(fs);
	printGLSLCompileLog( fs );
    glAttachShader( prog, vs );
    glAttachShader( prog, fs );
	// map positions inside the vertex buffer to shader variables
	glBindAttribLocation(prog, 0, "aPosition");
	glBindAttribLocation(prog, 1, "aNormal");
	glBindAttribLocation(prog, 3, "aColor");

	// prepare all of this for the calls in drawScene
    glLinkProgram( prog );	
	printGLSLLinkLog( prog );

    // ====================================================================
    // End Exercise code
    // ====================================================================
}

void initCustomResources()
{

    // =========================================================================
    // create your ressources here, e.g. buffers,...
    //
    // Code required for a,b,c
    //
    // Add your code here:
    // ====================================================================
	// vertex buffer for the GEOMETRY
	glGenBuffers(1, &bunny_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, bunny_vbo);
	glBufferData(GL_ARRAY_BUFFER, g_bunnyStrideSize * g_numberOfBunnyVertices,
		g_bunnyMesh, GL_STATIC_DRAW);
	// attributes:
	// vertices (4 floats)
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, g_bunnyStrideSize, 0); 
	// normals (3 floats)
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, g_bunnyStrideSize, (GLvoid*) (sizeof(float)*4));
	// texture coords (2 floats)
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, g_bunnyStrideSize, (GLvoid*) (sizeof(float)*7));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	//glEnableVertexAttribArray(2); // texture coordinates not used in this assignment

	// vertex buffer for the COLORS
	glGenBuffers(1, &bunny_color_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, bunny_color_vbo);
	glBufferData(GL_ARRAY_BUFFER, g_bunnyColorStrideSize * g_numberOfBunnyVertices,
		g_bunnyColor, GL_STATIC_DRAW);
	// single attribute: color. rescale to float
	glVertexAttribPointer(3, 3, GL_UNSIGNED_BYTE, GL_TRUE, g_bunnyColorStrideSize, 0);
	glEnableVertexAttribArray(3);


	
    // ====================================================================
    // End Exercise code
    // ====================================================================


	// create the shaders:
	createShaderProgram( vs_a, fs_a, prog_a, "shader_288857_a.vsh", "shader_288857_a.fsh" );
	createShaderProgram( vs_b, fs_b, prog_b, "shader_288857_b.vsh", "shader_288857_b.fsh" );
	createShaderProgram( vs_c, fs_c, prog_c, "shader_288857_c.vsh", "shader_288857_c.fsh" );
}

void deleteCustomResources()
{


    // ====================================================================
    // don't forget to delete your OpenGL ressources (shaders, buffers, etc.)!
    // Add your code here:
    // ====================================================================
	glDeleteShader(vs_a); glDeleteShader(fs_a); glDeleteProgram(prog_a);
	glDeleteShader(vs_b); glDeleteShader(fs_b); glDeleteProgram(prog_b);
	glDeleteShader(vs_c); glDeleteShader(fs_c); glDeleteProgram(prog_c);
	glDeleteBuffers(1, &bunny_vbo);
	glDeleteBuffers(1, &bunny_color_vbo);

    // ====================================================================
    // End Exercise code
    // ====================================================================
}

