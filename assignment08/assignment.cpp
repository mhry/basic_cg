/*
 * Basics of Computer Graphics Exercise
 */

#include "Tools/ABReader.hpp"
#include "Tools/PNGReader.hpp"
#include "Tools/TextureData.hpp"
#include "Tools/HelperFunctions.hpp"

#include "assignment.h"
using namespace std;

extern glm::mat4 g_ProjectionMatrix;

VertexArrayObject* g_vaoKilleroo;
ArrayBuffer*       g_abKilleroo;

ShaderProgram* shaderA; // you might need more than one program

GLuint diffuseTexture;
GLuint diffuseTextureSrgb;
GLuint specularTexture;

// add your team members names and matrikel numbers here:
void printStudents()
{
	cout << "Markus Hrywniak, 288857" << endl;
	cout << "Kaspar Scharf, 321503" << endl;
	cout << "Tobias Gerarts, 301447" << endl;
	cout << "Simon Oehrl, 310609" << endl;
}

void drawScene(int scene, float runTime) {

    // Create model-view matrix
    glm::mat4 viewMatrix   = g_camera.getViewMatrix();
    glm::mat4 modelMatrix  = glm::scale( glm::vec3(1.5f) );
    
    glm::mat4 modelViewMatrix    = viewMatrix * modelMatrix;
    glm::mat4 invTranspModelView = glm::inverse(glm::transpose(modelViewMatrix));

    shaderA->use();
    shaderA->setUniform("uProjectionMatrix",          g_ProjectionMatrix );
    shaderA->setUniform("uModelViewMatrix",           modelViewMatrix);
    shaderA->setUniform("uInvTranspModelViewMatrix",  invTranspModelView);
    shaderA->setUniform("uLightPosition",             glm::vec3( modelViewMatrix*vLightPosition) );
    shaderA->setUniform("uDiffuseTexture",            0);
    shaderA->setUniform("uSpecularTexture",           1);
	
	if (scene == 1) {
		// ignore the color space and use a RGB texture for the diffuse and ambient material
		// the specular material should be white (set as a constant in the fragment shader:
        shaderA->setUniform("uUseSpecularMap", 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseTexture);
        
        glDisable(GL_FRAMEBUFFER_SRGB);
		g_vaoKilleroo->render();
		
	} else if (scene == 2) {
		
		// use a sRGB texture for the diffuse and ambient material.
		// make sure the framebuffer is set to convert the linear content to sRGB!
		// the specular material should be white (set as a constant in the fragment shader:        
        shaderA->setUniform("uUseSpecularMap", 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseTextureSrgb);
        
        glEnable(GL_FRAMEBUFFER_SRGB);
		g_vaoKilleroo->render();
	} else if (scene == 3) {
		// use a sRGB texture for the diffuse and ambient material.
		// make sure the framebuffer is set to convert the linear content to sRGB!
		// the specular material should be read from a texture but should be gray scale (remember that this was defined in a linear space!)
		shaderA->setUniform("uUseSpecularMap", 1);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseTextureSrgb);
        
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, specularTexture);
        
        
        glEnable(GL_FRAMEBUFFER_SRGB);
		g_vaoKilleroo->render();
	}
}


void initCustomResources() {

    // Create your resources here, e.g. shaders, buffers,...

    ////////////////////////////////////////////////////////////////////////////
    // Shader:
    
    shaderA = new ShaderProgram("partA_310609.vsh", "partA_310609.fsh");
    if (!shaderA->link()) exit(0);

    // Set uniforms that don't change:
    shaderA->use();
    shaderA->setUniform( "uLightColor",             vLightColor);
    shaderA->setUniform( "uSpecularityExponent",    fSpecularityExponent);

    ////////////////////////////////////////////////////////////////////////////
    // Define geometry:

    ABReader abreader;
	g_abKilleroo  = abreader.readABFile("killeroo.ab");
    g_vaoKilleroo = new VertexArrayObject();
    g_vaoKilleroo->attachAllMatchingAttributes(g_abKilleroo, shaderA);

    ////////////////////////////////////////////////////////////////////////////
    // Read textures:
    
    PNGReader pngReader;
    TextureData* diffuseData = pngReader.readPNGFile("killerooDiffuse.png");
    
    glGenTextures(1, &diffuseTexture);
    glBindTexture(GL_TEXTURE_2D, diffuseTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
        diffuseData->getWidth(), diffuseData->getHeight(), 0,
        diffuseData->getFormat(), diffuseData->getType(), diffuseData->getData());
    glGenerateMipmap(GL_TEXTURE_2D);
    
    glGenTextures(1, &diffuseTextureSrgb);
    glBindTexture(GL_TEXTURE_2D, diffuseTextureSrgb);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB,
        diffuseData->getWidth(), diffuseData->getHeight(), 0,
        diffuseData->getFormat(), diffuseData->getType(), diffuseData->getData());
    glGenerateMipmap(GL_TEXTURE_2D);
    
    delete diffuseData;
    
    TextureData* specularData = pngReader.readPNGFile("killerooSpecular.png");
    
    glGenTextures(1, &specularTexture);
    glBindTexture(GL_TEXTURE_2D, specularTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,
        specularData->getWidth(), specularData->getHeight(), 0,
        specularData->getFormat(), specularData->getType(), specularData->getData());
    glGenerateMipmap(GL_TEXTURE_2D);
    
    delete specularData;
	
	glEnable(GL_DEPTH_TEST);
}

void deleteCustomResources() {

    // Don't forget to delete your OpenGL resources (shaders, buffers, etc.)!
    glDeleteTextures(1, &diffuseTexture);
    glDeleteTextures(1, &diffuseTextureSrgb);
    glDeleteTextures(1, &specularTexture);
    
    delete shaderA;
    delete g_vaoKilleroo;
    delete g_abKilleroo;
}

